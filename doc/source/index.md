# Welcome to fsl-pipe-gui's documentation!

terminal-based GUI for fsl-pipe

Install using

```
pip install git+https://git.fmrib.ox.ac.uk/ndcn0236/fsl-pipe-gui.git
```

```{toctree}
:maxdepth: 3

fsl-pipe-gui
```
